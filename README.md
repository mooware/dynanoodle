# dynanoodle

This is my repository for the [Coursera Cybersecurity Capstone Project](https://www.coursera.org/course/cybersecuritycapstone).

In case you're wondering about the repository name: It's randomly generated, from here: [http://www.dotomator.com/web20.html](http://www.dotomator.com/web20.html).
