#include "LogFile.h"
#include "global.h"
#include <type_traits>

namespace
{
  const unsigned int TYPE_BITS = 1;
  const unsigned int TYPE_MASK = 1;

  const unsigned int BOOL_BITS = 1;
  const unsigned int BOOL_MASK = 1;

  const unsigned int SIZE_BITS = 2;
  const unsigned int SIZE_MASK = (1 << SIZE_BITS) - 1;

  /// convert from stream status to file error code
  LogStatus convertErrCode(QDataStream::Status status)
  {
    switch (status)
    {
      case QDataStream::Ok:
        return LogStatus::Ok;
      case QDataStream::ReadPastEnd:
        return LogStatus::EndOfFile;
      case QDataStream::ReadCorruptData:
        return LogStatus::IntegrityError;
      default:
        return LogStatus::Invalid;
    }
  }

  /// write an enum value to QDataStream
  template <typename T>
  typename std::enable_if<std::is_enum<T>::value, QDataStream&>::type
  operator<<(QDataStream &stream, T value)
  {
    stream << static_cast<typename std::underlying_type<T>::type>(value);
    return stream;
  }

  /// read an enum value from QDataStream
  template <typename T>
  typename std::enable_if<std::is_enum<T>::value, QDataStream&>::type
  operator>>(QDataStream &stream, T &value)
  {
    typename std::underlying_type<T>::type tmp;
    stream >> tmp;
    value = static_cast<T>(tmp);
    return stream;
  }

  enum class IDSize : uint32_t { None, One, Two, Four };

  union IDType
  {
    uint8_t one;
    uint16_t two;
    uint32_t four;
  };

  template <typename T>
  typename std::enable_if<std::is_enum<T>::value, IDSize>::type
  getIDSize(T value)
  {
    using U = typename std::underlying_type<T>::type;
    if (U(value) == 0) return IDSize::None;
    if ((U(value) & 0xffffff00) == 0) return IDSize::One;
    if ((U(value) & 0xffff0000) == 0) return IDSize::Two;
    return IDSize::Four;
  }

  /// write an ID value with 1, 2 or 4 bytes
  template <typename T>
  void writeID(QDataStream &stream, IDSize size, T value)
  {
    switch (size)
    {
      case IDSize::None: break;
      case IDSize::One:  stream << static_cast<uint8_t>(value); break;
      case IDSize::Two:  stream << static_cast<uint16_t>(value); break;
      case IDSize::Four: stream << static_cast<uint32_t>(value); break;
    }
  }

  /// read an ID value with 1, 2 or 4 bytes
  template <typename T>
  T readID(QDataStream &stream, IDSize size)
  {
    IDType id;
    switch (size)
    {
      case IDSize::None: break;
      case IDSize::One:  stream >> id.one; return static_cast<T>(id.one);
      case IDSize::Two:  stream >> id.two; return static_cast<T>(id.two);
      case IDSize::Four: stream >> id.four; return static_cast<T>(id.four);
    }

    return static_cast<T>(0);
  }
}

LogFile::LogFile(const char *key)
  : crypto(key)
{
}

LogFile::~LogFile()
{
  flush();
}

LogStatus LogFile::flush()
{
  if (mode == OpenMode::ReadOnly || writeBuffer.size() <= 0)
    return LogStatus::Invalid;

  // have to rewrite old data too
  QByteArray writeData = readBuffer.data();
  writeData += writeBuffer.data();

#ifdef DYNANOODLE_ZIP
  // simple compression
  writeData = qCompress(writeData);
#endif // DYNANOODLE_ZIP

  QByteArray newData;
  if (!crypto.encrypt(writeData.constData(), writeData.size(), newData))
    return LogStatus::IntegrityError;

  QFile file(this->path);
  if (!file.open(QFile::WriteOnly))
    return LogStatus::Invalid;

  file.write(newData);
  return LogStatus::Ok;
}

LogStatus LogFile::open(const char *path, OpenMode mode)
{
  this->path = QFile::decodeName(path);
  this->mode = mode;

  readBuffer.open(QBuffer::ReadOnly);
  writeBuffer.open(QBuffer::WriteOnly);
  readStream.setDevice(&readBuffer);
  writeStream.setDevice(&writeBuffer);

  QFile file(this->path);
  if (!file.open(QFile::ReadOnly))
  {
    if (mode == OpenMode::ReadOnly)
      return LogStatus::FileNotFound;

    if (!crypto.init())
      return LogStatus::IntegrityError;
  }
  else
  {
    QByteArray encData = file.readAll();
    QByteArray decData;
    if (!crypto.init(encData.constData(), encData.size(), decData))
      return LogStatus::IntegrityError;

    bool hasData = !decData.isEmpty();
#ifdef DYNANOODLE_ZIP
    // simple compression
    decData = qUncompress(decData);
#endif // DYNANOODLE_ZIP
    if (hasData && decData.isEmpty())
      return LogStatus::IntegrityError; // gzip integrity

    readBuffer.buffer() = decData;
  }

  return LogStatus::Ok;
}

LogStatus LogFile::readType(RecordType &type)
{
  readStream >> readBits;
  type = RecordType(readBits & TYPE_MASK);
  readBits >>= TYPE_BITS; // remove type bit
  return convertErrCode(readStream.status());
}

LogStatus LogFile::readPerson(PersonRecord &record)
{
  // read bits (lowest to highest): isGuest, size of record.id
  record.isGuest = bool(readBits & BOOL_MASK);
  IDSize size = IDSize((readBits >> BOOL_BITS) & SIZE_MASK);

  record.id = readID<decltype(record.id)>(readStream, size);
  readStream >> record.name;

  DEBUGMSG("read person (" << readBuffer.pos() << "): " << record.id << ", " << record.name.constData());
  return convertErrCode(readStream.status());
}

LogStatus LogFile::readEvent(EventRecord &record)
{
  // read bits (lowest to highest):
  // isGallery, isArrival
  record.isGallery = bool(readBits & BOOL_MASK);
  record.isArrival = bool((readBits >> BOOL_BITS) & BOOL_MASK);

  // read bits (lowest to highest):
  // size of time, size of person, size of room
  readStream >> readBits;

  IDSize timeSize = IDSize(readBits & SIZE_MASK);
  record.time = readID<Timestamp>(readStream, timeSize);

  readBits >>= SIZE_BITS;
  IDSize personSize = IDSize(readBits & SIZE_MASK);
  record.person = readID<PersonID>(readStream, personSize);

  if (record.isGallery)
  {
    record.room = RoomID(0);
  }
  else
  {
    readBits >>= SIZE_BITS;
    IDSize roomSize = IDSize(readBits & SIZE_MASK);
    record.room = readID<RoomID>(readStream, roomSize);
  }

  DEBUGMSG("read event (" << readBuffer.pos() << "): at " << enum_value(record.time)
           << " in " << enum_value(record.room) << (record.isGallery ? " (gallery)" : "") << ", "
           << enum_value(record.person) << (record.isArrival ? " arrives" : " leaves"));
  return convertErrCode(readStream.status());
}

LogStatus LogFile::writePerson(const PersonRecord &record)
{
  IDSize idSize = getIDSize(record.id);
  // store bits (lowest to highest): type=Person, isGuest, size of id
  uint8_t writeBits = uint8_t(idSize);
  writeBits = ((writeBits << BOOL_BITS) | record.isGuest);
  writeBits  = ((writeBits << TYPE_BITS) | uint8_t(RecordType::Person));

  writeStream << writeBits;
  writeID(writeStream, idSize, record.id);
  writeStream << record.name;

  DEBUGMSG("write person: " << record.id << ", " << record.name.constData());
  return convertErrCode(writeStream.status());
}

LogStatus LogFile::writeEvent(const EventRecord &record)
{
  // store bits (lowest to highest):
  // type=Event, isGallery, isArrival
  uint8_t writeBits;
  writeBits = uint8_t(record.isArrival);
  writeBits = ((writeBits << BOOL_BITS) | record.isGallery);
  writeBits = ((writeBits << TYPE_BITS) | uint8_t(RecordType::Event));

  writeStream << writeBits;

  // store bits (lowest to highest):
  // size of time, size of person, size of room
  IDSize roomSize = getIDSize(record.room);
  writeBits = uint8_t(roomSize);

  IDSize personSize = getIDSize(record.person);
  writeBits = ((writeBits << SIZE_BITS) | uint8_t(personSize));

  IDSize timeSize = getIDSize(record.time);
  writeBits = ((writeBits << SIZE_BITS) | uint8_t(timeSize));

  writeStream << writeBits;
  writeID(writeStream, timeSize, record.time);
  writeID(writeStream, personSize, record.person);
  if (!record.isGallery) // can save a few bits for gallery events
    writeID(writeStream, roomSize, record.room);

  DEBUGMSG("write event: at " << enum_value(record.time)
           << " in " << enum_value(record.room) << (record.isGallery ? " (gallery)" : "") << ", "
           << enum_value(record.person) << (record.isArrival ? " arrives" : " leaves"));
  return convertErrCode(writeStream.status());
}
