#ifndef DYNANOODLE_CRYPTOBUFFER_H
#define DYNANOODLE_CRYPTOBUFFER_H

#include <QByteArray>
#include <QIODevice>
#include <memory>

class CryptoBuffer
{
public:
  CryptoBuffer(const char *pass);
  ~CryptoBuffer();

  /// initialize an empty crypto buffer
  bool init();
  /// initialize a crypto buffer based on existing encrypted data
  bool init(const char *encData, int len, QByteArray &decData);
  /// encrypt the given data
  bool encrypt(const char *decData, int len, QByteArray &encData);

private:
  /// en/decrypt the given data
  bool doCrypt(const char *in, int inlen, bool isEncrypt, QByteArray &out);

  class Private;
  std::unique_ptr<Private> d;

};

#endif //DYNANOODLE_CRYPTOBUFFER_H
