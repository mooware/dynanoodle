#include "LogManager.h"

LogManager::LogManager(const char *path, const char *key, Mode mode)
  : file(key), nextPersonId(PersonID(0)), lastTimestamp(Timestamp(0))
{
  auto filemode = (mode == Mode::Append ? LogFile::OpenMode::ReadWrite : LogFile::OpenMode::ReadOnly);
  openStatus_ = file.open(path, filemode);
}

LogStatus LogManager::addEvent(Timestamp timestamp, const PersonRecord &person, ArriveLeaveMode mode)
{
  return addEventImpl(timestamp, person, mode, false, RoomID(0));
}

LogStatus LogManager::addEvent(Timestamp timestamp, const PersonRecord &person, ArriveLeaveMode mode, RoomID room)
{
  return addEventImpl(timestamp, person, mode, true, room);
}

LogStatus LogManager::addEventImpl(Timestamp timestamp, const PersonRecord &person, ArriveLeaveMode mode,
                             bool isRoom, RoomID room)
{
  if (openStatus_ != LogStatus::Ok)
    return openStatus_;

  // check timestamp
  if (lastTimestamp > 0 && timestamp <= lastTimestamp)
    return DEBUGRET(LogStatus::Invalid, "timestamp must increase");

  // check guest/employee, get person id
  bool isNewPerson;
  PersonID pid;
  auto &names = nameMap(person.isGuest);
  auto nameIt = names.find(person.name);
  if (nameIt != names.end())
  {
    // person is known -> integrity checks
    isNewPerson = false;
    pid = *nameIt;
    auto idIt = people.find(pid);
    if (idIt->isGuest != person.isGuest)
      return DEBUGRET(LogStatus::Invalid,
                      "guest/employee mismatch");
  }
  else
  {
    isNewPerson = true;
    pid = PersonID(nextPersonId);
  }

  // arrive/leave integrity checks
  if (mode == ArriveLeaveMode::Arrive && !isRoom)
  {
    // arrive in gallery -> can't be in gallery already (excludes rooms implicitly)
    if (gallery.find(pid) != gallery.end())
      return DEBUGRET(LogStatus::Invalid,
                      "arrive in gallery -> can't be in gallery already");
  }
  else if ((mode == ArriveLeaveMode::Arrive && isRoom) ||
           (mode == ArriveLeaveMode::Leave && !isRoom))
  {
    // arrive in room or leave gallery -> must be in gallery, can't be in rooms
    if (gallery.find(pid) == gallery.end() || peopleInRooms.contains(pid))
      return DEBUGRET(LogStatus::Invalid,
                      "arrive in room or leave gallery -> must be in gallery, can't be in rooms");
  }
  else if (mode == ArriveLeaveMode::Leave && isRoom)
  {
    // leave room -> must be in same room (and in gallery implicitly)
    auto it = peopleInRooms.find(pid);
    if (it == peopleInRooms.end() || *it != room)
      return DEBUGRET(LogStatus::Invalid,
                      "leave room -> must be in same room (and in gallery implicitly)");
  }
  else // mode == None?
  {
    return LogStatus::Invalid;
  }

  // add new person?
  PersonRecord newPerson;
  if (isNewPerson)
  {
    newPerson = person;
    newPerson.id = PersonID(nextPersonId);

    auto res = file.writePerson(newPerson);
    if (res != LogStatus::Ok)
      return LogStatus::Invalid;

    ++nextPersonId;
    people.insert(pid, newPerson);
    names.insert(newPerson.name, pid);
  }

  EventRecord event = { timestamp, pid, room, !isRoom, mode == ArriveLeaveMode::Arrive };
  auto res = file.writeEvent(event);
  if (res != LogStatus::Ok)
    return LogStatus::Invalid;

  update(event);
  return LogStatus::Ok;
}

void LogManager::update(const EventRecord &event)
{
  auto &room = (event.isGallery ? gallery : rooms[event.room]);
  if (event.isArrival)
  {
    room.insert(event.person);
    if (event.isGallery)
    {
      galleryEnterTime[event.person] = event.time;
    }
    else
    {
      peopleInRooms[event.person] = event.room;
      visitedRooms_[event.person].append(event.room);
    }
  }
  else
  {
    room.erase(event.person);
    if (event.isGallery)
    {
      Timestamp prev = galleryEnterTime.take(event.person);
      Duration current = Duration(event.time) - Duration(prev);
      visitDurations[event.person] += current;
    }
    else
      peopleInRooms.remove(event.person);
  }

  lastTimestamp = event.time;
}

const PersonRecord &LogManager::person(PersonID id) const
{
  auto it = people.find(id);
  if (it != people.end())
    return *it;

  // shouldn't be reached
  static PersonRecord dummy;
  return dummy;
}

std::pair<PersonID, bool> LogManager::personId(const QByteArray &name, bool isGuest) const
{
  const auto &names = nameMap(isGuest);
  auto it = names.find(name);
  if (it == names.end())
    return std::make_pair(PersonID(0), false);
  else
    return std::make_pair(*it, true);
}

std::pair<RoomID, bool> LogManager::personInRoom(PersonID id) const
{
  auto it = peopleInRooms.find(id);
  if (it == peopleInRooms.end())
    return std::make_pair(RoomID(0), false);
  else
    return std::make_pair(*it, true);
}

LogManager::Duration LogManager::visitDuration(PersonID id) const
{
  Duration d = visitDurations[id];
  // still in gallery? add current duration
  auto it = galleryEnterTime.find(id);
  if (it != galleryEnterTime.end())
    d += (Duration(lastTimestamp) - Duration(*it));
  return d;
}

LogStatus LogManager::readAllEvents()
{
  LogStatus res;
  while ((res = readNextEvent()) == LogStatus::Ok)
    ;
  return res;
}

LogStatus LogManager::readNextEvent()
{
  if (openStatus_ != LogStatus::Ok)
    return openStatus_;

  RecordType type;
  LogStatus res = file.readType(type);
  if (res != LogStatus::Ok)
    return res;

  if (type == RecordType::Person)
  {
    // read record
    PersonRecord person;
    res = file.readPerson(person);
    if (res != LogStatus::Ok)
      return res;

    // integrity checks
    auto &names = nameMap(person.isGuest);
    if (people.contains(person.id) || names.contains(person.name))
      return LogStatus::IntegrityError;

    // insert data
    nextPersonId = person.id + 1;
    people.insert(person.id, person);
    names.insert(person.name, person.id);

    // this wasn't an event, so read again
    return readNextEvent();
  }
  else if (type == RecordType::Event)
  {
    EventRecord &event = lastReadEvent;
    res = file.readEvent(event);
    if (res != LogStatus::Ok)
      return res;

    // integrity checks
    auto idIt = people.find(event.person);
    if (idIt == people.end())
      return LogStatus::IntegrityError;

    // insert data
    update(event);
    return LogStatus::Ok;
  }

  // unknown type -> error
  return LogStatus::IntegrityError;
}
