#include "LogManager.h"
#include "global.h"
#include <iostream>
#include <memory>
#include <cstring>
#include <QByteArray>
#include <QString>
#include <QFile>
#include <QTextStream>
#include <QMap>
#include <QVector>

namespace logappend
{
  using LogCache = QMap<QPair<QByteArray, QByteArray>, LogManager*>;

  struct Args
  {
    bool hasTimestamp = false;
    Timestamp timestamp;
    bool hasRoom = false;
    RoomID room;
    QByteArray token;
    PersonRecord person;
    ArriveLeaveMode mode = ArriveLeaveMode::None;
    QByteArray path;
  };

  bool parse(int argc, char **argv, Args &args)
  {
    for (int i = 0; i < argc; ++i)
    {
      const char *opt = argv[i];

      if (strcmp("-T", opt) == 0 && i+1 < argc)
      {
        args.timestamp = (Timestamp) QByteArray(argv[++i]).toUInt(&args.hasTimestamp);
        if (!args.hasTimestamp || !validateTime(args.timestamp))
          return DEBUGRET(false, "invalid timestamp: " << argv[i]);
      }
      else if (strcmp("-K", opt) == 0 && i+1 < argc)
      {
        const char *token = argv[++i];
        args.token = token;
        if (!validateToken(token))
          return DEBUGRET(false, "invalid token: " << token);
      }
      else if ((strcmp("-E", opt) == 0 || strcmp("-G", opt) == 0) && i+1 < argc)
      {
        const char *name = argv[++i];
        if (!validateName(name))
          return DEBUGRET(false, "invalid name: " << name);
        args.person.isGuest = (strcmp("-G", opt) == 0);
        args.person.name = name;
        args.person.id = PersonID(0);
      }
      else if (strcmp("-A", opt) == 0)
      {
        args.mode = ArriveLeaveMode::Arrive;
      }
      else if (strcmp("-L", opt) == 0)
      {
        args.mode = ArriveLeaveMode::Leave;
      }
      else if (strcmp("-R", opt) == 0 && i+1 < argc)
      {
        args.room = (RoomID) QByteArray(argv[++i]).toUInt(&args.hasRoom);
        if (!args.hasRoom || !validateRoom(args.room))
          return false;
      }
      else if (*opt == '-') // unknown option
      {
        return DEBUGRET(false, "unknown option: " << opt);
      }
      else // log file
      {
        if (!args.path.isEmpty())
          return DEBUGRET(false, "path already specified");
        args.path = opt;
      }
    }

    // check mandatory values
    return (args.hasTimestamp && !args.token.isEmpty() && !args.person.name.isEmpty() &&
            args.mode != ArriveLeaveMode::None && !args.path.isEmpty());
  }

  int processCommand(int argc, char **argv, LogCache *cache = nullptr)
  {
    LogStatus res = LogStatus::Ok;
    Args args;
    if (!parse(argc, argv, args))
      res = LogStatus::Invalid;

    if (res == LogStatus::Ok)
    {
      // check if file is already cached
      auto cacheKey = qMakePair(args.path, args.token);
      LogManager *log = (cache ? cache->value(cacheKey, nullptr) : nullptr);
      if (!log)
      {
        log = new LogManager(args.path.constData(), args.token.constData(), LogManager::Mode::Append);
        res = log->openStatus();
        if (res == LogStatus::Ok)
          res = log->readAllEvents();
      }

      if (res == LogStatus::Ok || res == LogStatus::EndOfFile)
      {
        if (args.hasRoom)
          res = log->addEvent(args.timestamp, args.person, args.mode, args.room);
        else
          res = log->addEvent(args.timestamp, args.person, args.mode);
      }

      // (re-)store in cache, or in case of error drop cache, just to be sure
      if (cache)
      {
        if (res == LogStatus::Ok)
          (*cache)[cacheKey] = log;
        else
          cache->remove(cacheKey);
      }

      // no cache or error, delete
      if (!cache || res != LogStatus::Ok)
        delete log;
    }

    if (res == LogStatus::Ok)
      return 0;

    std::cout << "invalid" << std::endl;
    return ERR_INVALID;
  }

  int readCommandFile(const char *path)
  {
    DEBUGMSG("reading command file " << path);

    QFile file(QFile::decodeName(path));
    if (!file.open(QFile::ReadOnly))
    {
      std::cout << "invalid" << std::endl;
      return ERR_INVALID;
    }

    LogCache cache;
    QVector<char*> argv;
    QTextStream stream(&file);
    while (!stream.atEnd())
    {
      QString line = stream.readLine();
      if (line.isEmpty())
        continue;
      auto args = line.toUtf8().split(' ');
      argv.resize(args.size() + 1);

      int argc = 0;
      for (auto &arg : args)
        argv[argc++] = arg.data();
      argv[argc] = nullptr;

      processCommand(argc, argv.data(), &cache);
    }

    qDeleteAll(cache); // clean up cached files
    return 0;
  }

  int main(int argc, char **argv)
  {
    DEBUGMSG("starting logappend");

    if (argc < 3)
      return DEBUGRET(ERR_INVALID, "help:" << std::endl <<
                      "logappend -T <timestamp> -K <token> (-E <employee-name> | -G <guest-name>) (-A | -L) [-R <room-id>] <log>" << std::endl <<
                      "logappend -B <file>");

    if (strcmp("-B", argv[1]) == 0)
      return readCommandFile(argv[2]);
    else
      return processCommand(argc - 1, argv + 1);
  }

} // namespace logappend