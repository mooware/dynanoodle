#ifndef DYNANOODLE_LOGFILE_H
#define DYNANOODLE_LOGFILE_H

#include "global.h"
#include "CryptoBuffer.h"
#include <QBuffer>
#include <QFile>
#include <QDataStream>

class LogFile
{
public:
  enum class OpenMode { ReadOnly, ReadWrite };

  LogFile(const char *key);
  ~LogFile();

  LogFile(const LogFile &) = delete;
  LogFile &operator=(const LogFile &) = delete;

  LogStatus open(const char *path, OpenMode mode = OpenMode::ReadOnly);
  LogStatus flush();

  LogStatus readType(RecordType &type);
  LogStatus readPerson(PersonRecord &record);
  LogStatus readEvent(EventRecord &record);

  LogStatus writePerson(const PersonRecord &record);
  LogStatus writeEvent(const EventRecord &record);

private:
  QString path;
  OpenMode mode;

  QDataStream readStream;
  QDataStream writeStream;
  QBuffer readBuffer;
  QBuffer writeBuffer;
  CryptoBuffer crypto;
  // additional bits stored in last read type
  uint8_t readBits;

};


#endif // DYNANOODLE_LOGFILE_H
