#ifndef DYNANOODLE_LOGMANAGER_H
#define DYNANOODLE_LOGMANAGER_H

#include "LogFile.h"
#include "global.h"
#include <QMap>
#include <QVector>
#include <set>
#include <type_traits>
#include <utility>

class LogManager
{
public:
  enum class Mode { Read, Append };

  /// type for people in room
  using PeopleSet = std::set<PersonID>;
  /// type for visited rooms
  using RoomList = QVector<RoomID>;
  /// duration of visit
  using Duration = std::underlying_type<Timestamp>::type;

  LogManager(const char *path, const char *key, Mode mode);
  LogStatus openStatus() const { return openStatus_; }

  LogStatus addEvent(Timestamp timestamp, const PersonRecord &person, ArriveLeaveMode mode);
  LogStatus addEvent(Timestamp timestamp, const PersonRecord &person, ArriveLeaveMode mode, RoomID room);

  LogStatus readAllEvents();
  LogStatus readNextEvent();

  const PersonRecord &person(PersonID id) const;
  std::pair<PersonID, bool> personId(const QByteArray &name, bool isGuest) const;
  std::pair<RoomID, bool> personInRoom(PersonID id) const;

  const PeopleSet &galleryVisitors() const { return gallery; }
  const QMap<RoomID, PeopleSet> &roomVisitors() const { return rooms; }
  const QMap<PersonID, RoomList> &visitedRooms() const { return visitedRooms_; }

  Duration visitDuration(PersonID id) const;

private:

  LogStatus addEventImpl(Timestamp timestamp, const PersonRecord &person, ArriveLeaveMode mode, bool isRoom, RoomID room);
  void update(const EventRecord &event);

  QMap<QByteArray, PersonID>&nameMap(bool isForGuests) { return (isForGuests ? guestsByName : employeesByName); }
  const QMap<QByteArray, PersonID> &nameMap(bool isForGuests) const { return (isForGuests ? guestsByName : employeesByName); }

  LogFile file;
  LogStatus openStatus_;

  std::underlying_type<PersonID>::type nextPersonId;
  Timestamp lastTimestamp;
  EventRecord lastReadEvent;

  /// map from person id to record
  QMap<PersonID, PersonRecord> people;
  /// map from guest name to id
  QMap<QByteArray, PersonID> guestsByName;
  /// map from employee name to id
  QMap<QByteArray, PersonID> employeesByName;
  /// in which room is a person currently?
  QMap<PersonID, RoomID> peopleInRooms;

  /// people currently in the gallery
  PeopleSet gallery;
  /// rooms and the people currently in them
  QMap<RoomID, PeopleSet> rooms;
  /// rooms visited by a person
  QMap<PersonID, RoomList> visitedRooms_;
  /// how long did a person spend in the gallery (excluding the current visit)
  QMap<PersonID, Duration> visitDurations;
  /// last time a person entered the gallery (if currently in the gallery)
  QMap<PersonID, Timestamp> galleryEnterTime;
};

#endif //DYNANOODLE_LOGMANAGER_H
