#ifndef DYNANOODLE_TYPES_H
#define DYNANOODLE_TYPES_H

#include <QByteArray>
#include <cstdint>
#include <type_traits>

/// exit code for any kind of error
const int ERR_INVALID = 255;

enum Timestamp : uint32_t { };
enum PersonID : uint32_t { };
enum RoomID : uint32_t { };
enum class RecordType : uint8_t { Person, Event };

enum class ArriveLeaveMode { None, Arrive, Leave };

enum class LogStatus { Ok, FileNotFound, EndOfFile, IntegrityError, Invalid };

struct PersonRecord
{
  QByteArray name;
  PersonID id;
  bool isGuest;
};

struct EventRecord
{
  Timestamp time;
  PersonID person;
  RoomID room;
  bool isGallery;
  bool isArrival;
};

/// extract the underlying value of an enum member
template <typename T>
typename std::enable_if<std::is_enum<T>::value,
                        typename std::underlying_type<T>::type>::type
enum_value(T value)
{
  return static_cast<typename std::underlying_type<T>::type>(value);
}

template <typename T>
class ScopeGuard : public T
{
public:
  ScopeGuard(T&& callable)
    : T(std::forward<T>(callable)), released(false)
  {
  }

  ~ScopeGuard() { clear(); }

  void release() { released = true; }

  void clear()
  {
    if (!released)
    {
      (*this)();
      released = true;
    }
  }

private:
  bool released;
};

template <typename T>
ScopeGuard<T> makeGuard(T&& callable)
{
  return ScopeGuard<T>(std::forward<T>(callable));
}

/// check a name for valid characters
bool validateName(const char *name);
/// check a token for valid characters
bool validateToken(const char *token);
/// check valid range of timestamp
bool validateTime(Timestamp value);
/// check valid range of room ID
bool validateRoom(RoomID value);

/// helper macro for output
#define PRINT(...) (std::cout << __VA_ARGS__)
#define PRINTLN(...) PRINT(__VA_ARGS__ << std::endl)

/// helper macro for debug output
#ifdef NDEBUG
#define DEBUGMSG(...) (void)0
#define DEBUGRET(ret, ...) ret
#else // NDEBUG
#include <iostream>
#define DEBUGMSG(...) (std::cerr << "dbg: " << __VA_ARGS__ << std::endl)
#define DEBUGRET(ret, ...) (DEBUGMSG(__VA_ARGS__), ret)
#endif // NDEBUG

#endif //DYNANOODLE_TYPES_H
