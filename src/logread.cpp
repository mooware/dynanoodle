#include "global.h"
#include "LogManager.h"
#include <QByteArray>
#include <QVector>
#include <iostream>
#include <cstring>
#include <set>
#include <type_traits>

template <typename Container, typename Converter>
void printJoined(const Container &container, Converter&& conv)
{
  bool first = true;
  for (const auto &item : container)
  {
    if (first)
      first = false;
    else
      std::cout << ',';
    std::cout << conv(item);
  }
  std::cout << std::endl;
}

template <typename Container>
void printJoined(const Container &container)
{
  using ItemType = decltype(container[0]);
  printJoined(container, [](const ItemType &x){ return x; });
}

namespace logread
{
  enum class ReadMode { None, ShowLog, RoomsEntered, TimeOfVisit, InRooms };

  struct Args
  {
    QByteArray token;
    QVector<PersonRecord> people;
    QByteArray path;
    ReadMode mode = ReadMode::None;
  };

  bool parse(int argc, char **argv, Args &args)
  {
    for (int i = 0; i < argc; ++i)
    {
      const char *opt = argv[i];

      if (strcmp("-K", opt) == 0 && i + 1 < argc)
      {
        const char *token = argv[++i];
        args.token = token;
        if (!validateToken(token))
          return DEBUGRET(false, "invalid token: " << token);
      }
      else if (strcmp("-S", opt) == 0)
      {
        args.mode = ReadMode::ShowLog;
      }
      else if (strcmp("-R", opt) == 0)
      {
        args.mode = ReadMode::RoomsEntered;
      }
      else if (strcmp("-T", opt) == 0)
      {
        args.mode = ReadMode::TimeOfVisit;
      }
      else if (strcmp("-I", opt) == 0)
      {
        args.mode = ReadMode::InRooms;
      }
      else if ((strcmp("-E", opt) == 0 || strcmp("-G", opt) == 0) && i+1 < argc)
      {
        const char *name = argv[++i];
        if (!validateName(name))
          return DEBUGRET(false, "invalid name: " << name);
        PersonRecord p;
        p.isGuest = (strcmp("-G", opt) == 0);
        p.name = name;
        p.id = PersonID(0);
        args.people.append(p);
      }
      else if (*opt == '-') // unknown option
      {
        return DEBUGRET(false, "unknown option: " << opt);
      }
      else // log file
      {
        if (!args.path.isEmpty())
          return DEBUGRET(false, "path already specified");
        args.path = argv[i];
      }
    }

    // check mandatory values
    if (args.mode == ReadMode::None || args.path.isEmpty())
      return DEBUGRET(false, "mode or path missing");

    if ((args.mode == ReadMode::RoomsEntered || args.mode == ReadMode::TimeOfVisit) &&
        args.people.size() != 1)
      return DEBUGRET(false, "too many people specified");

    if (args.mode == ReadMode::InRooms && args.people.isEmpty())
      return DEBUGRET(false, "no people specified");

    return true;
  }

  LogStatus showLog(const Args &args)
  {
    DEBUGMSG("-S: show log");

    LogManager log(args.path.constData(), args.token.constData(), LogManager::Mode::Read);
    LogStatus res = log.readAllEvents();
    if (res != LogStatus::EndOfFile)
      return res;

    // separate employees and guests
    QVector<QByteArray> employee;
    QVector<QByteArray> guest;
    for (auto id : log.galleryVisitors())
    {
      const auto &rec = log.person(id);
      if (rec.isGuest)
        guest.append(rec.name);
      else
        employee.append(rec.name);
    }

    auto printQByteArray = [](const QByteArray &str) { return str.constData(); };
    qSort(employee);
    printJoined(employee, printQByteArray);
    qSort(guest);
    printJoined(guest, printQByteArray);

    QMapIterator<RoomID, LogManager::PeopleSet> it(log.roomVisitors());
    while (it.hasNext())
    {
      it.next();
      // don't print empty rooms
      if (it.value().empty())
        continue;
      QVector<QByteArray> names;
      for (auto id : it.value())
        names.append(log.person(id).name);
      qSort(names);
      std::cout << it.key() << ": ";
      printJoined(names, printQByteArray);
    }

    return LogStatus::Ok;
  }

  LogStatus roomsEntered(const Args &args)
  {
    DEBUGMSG("-R: rooms entered");

    LogManager log(args.path.constData(), args.token.constData(), LogManager::Mode::Read);
    LogStatus res = log.readAllEvents();
    if (res != LogStatus::EndOfFile)
      return res;

    const auto &argPerson = args.people.first();
    auto personId = log.personId(argPerson.name, argPerson.isGuest);
    if (!personId.second)
      return LogStatus::Ok; // person not found

    const auto &person = log.person(personId.first);
    if (person.isGuest != args.people.first().isGuest)
      return LogStatus::Ok; // guest mismatch (i.e. not found?)

    auto rooms = log.visitedRooms()[person.id];
    printJoined(rooms);
    return LogStatus::Ok;
  }

  LogStatus timeOfVisit(const Args &args)
  {
    DEBUGMSG("-T: time of visit");

    LogManager log(args.path.constData(), args.token.constData(), LogManager::Mode::Read);
    LogStatus res = log.readAllEvents();
    if (res != LogStatus::EndOfFile)
      return res;

    const auto &argPerson = args.people.first();
    auto personId = log.personId(argPerson.name, argPerson.isGuest);
    if (!personId.second)
      return LogStatus::Ok; // person not found

    const auto &person = log.person(personId.first);
    if (person.isGuest != args.people.first().isGuest)
      return LogStatus::Ok; // guest mismatch (i.e. not found?)

    std::cout << log.visitDuration(person.id) << std::endl;
    return LogStatus::Ok;
  }

  LogStatus inRooms(const Args &args)
  {
    DEBUGMSG("-I: in rooms");

    // have to do two passes, first to get all person ids, then to find rooms
    std::set<PersonID> people;
    LogStatus res;

    {
      LogManager log(args.path.constData(), args.token.constData(), LogManager::Mode::Read);
      res = log.readAllEvents();
      if (res != LogStatus::EndOfFile)
        return res;

      for (const auto &p : args.people)
      {
        auto personId = log.personId(p.name, p.isGuest);
        if (!personId.second)
          continue;

        const auto &person = log.person(personId.first);
        if (person.isGuest != args.people.first().isGuest)
          continue;

        people.insert(person.id);
      }
    }

    if (people.empty())
      return LogStatus::Ok;

    // second pass
    PersonID firstPerson = *people.begin();
    std::set<RoomID> rooms;
    LogManager log(args.path.constData(), args.token.constData(), LogManager::Mode::Read);
    while ((res = log.readNextEvent()) == LogStatus::Ok)
    {
      // check in which room the first person currently is,
      // then whether the other people are in the same room
      auto firstPersonRoom = log.personInRoom(firstPerson);
      if (!firstPersonRoom.second)
        continue;

      auto room = firstPersonRoom.first;
      const auto &visitors = log.roomVisitors();
      auto it = visitors.find(room);
      if (it == visitors.end())
        continue;

      bool hasAllPeople = true;
      auto endIt = it->end();
      for (auto p : people)
      {
        if (it->find(p) == endIt)
        {
          hasAllPeople = false;
          break;
        }
      }

      if (hasAllPeople)
        rooms.insert(room);
    }

    if (res == LogStatus::EndOfFile && !rooms.empty())
      printJoined(rooms, [](RoomID id) { return std::underlying_type<RoomID>::type(id); });

    if (res == LogStatus::EndOfFile)
      return LogStatus::Ok;
    else
      return res;
  }

  int processCommand(int argc, char **argv)
  {
    Args args;
    if (!parse(argc, argv, args))
      return DEBUGRET(ERR_INVALID, "parsing failed");

    LogStatus res;
    switch (args.mode)
    {
      case ReadMode::ShowLog:
        res = showLog(args);
        break;
      case ReadMode::RoomsEntered:
        res = roomsEntered(args);
        break;
      case ReadMode::TimeOfVisit:
        res = timeOfVisit(args);
        break;
      case ReadMode::InRooms:
        res = inRooms(args);
        break;
      default:
        res = LogStatus::Invalid;
    }

    return (res == LogStatus::Ok ? 0 : 255);
  }

  int main(int argc, char **argv)
  {
    if (argc < 5)
      return DEBUGRET(ERR_INVALID, "help:" << std::endl <<
                      "logread -K <token> -S <log>" << std::endl <<
                      "logread -K <token> -R (-E <name> | -G <name>) <log>" << std::endl <<
                      "logread -K <token> -T (-E <name> | -G <name>) <log>" << std::endl <<
                      "logread -K <token> -I (-E <name> | -G <name>) [(-E <name> | -G <name>) ...] <log>");

    int res = processCommand(argc - 1, argv + 1);
    if (res != 0)
      std::cout << "invalid" << std::endl;
    return res;
  }
}