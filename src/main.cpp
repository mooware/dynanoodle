#include "global.h"
#include <cstring>

/// fwd decls for tool-specific main functions
namespace logread { int main(int argc, char **argv); }
namespace logappend { int main(int argc, char **argv); }

/// real main function
int main(int argc, char **argv)
{
  if (argc < 1)
    return ERR_INVALID;

  // parse away any leading path segments
  const char *progname = argv[0];
  for (const char *p = progname; *p; ++p)
    if (*p == '/')
      progname = p+1;

  // busybox-like multi-dispatch
  if (strcmp(progname, "logread") == 0)
    return logread::main(argc, argv);
  else if (strcmp(progname, "logappend") == 0)
    return logappend::main(argc, argv);
  else // maybe called like "dynanoodle logread ..."
    return main(argc - 1, argv + 1);
};