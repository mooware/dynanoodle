#include "CryptoBuffer.h"

#include "global.h"
#include <openssl/bio.h>
#include <openssl/evp.h>
#include <openssl/rand.h>
#include <cstring>

static const int PBKDF_ITERATIONS = 1000;

class CryptoBuffer::Private
{
public:
  QByteArray password;

  const EVP_CIPHER *cipher;
  const EVP_MD *digest;

  unsigned char keySalt[PKCS5_SALT_LEN];
  unsigned char ivSalt[PKCS5_SALT_LEN];
  unsigned char key[EVP_MAX_KEY_LENGTH];
  unsigned char iv[EVP_MAX_IV_LENGTH];
};

CryptoBuffer::CryptoBuffer(const char *pass)
  : d(new Private())
{
  d->password = pass;
  d->cipher = EVP_aes_256_cbc();
  d->digest = EVP_sha384();
}

CryptoBuffer::~CryptoBuffer()
{
  // unique_ptr member dtor is generated here
}

/// initialize an empty crypto buffer
bool CryptoBuffer::init()
{
  QByteArray dummy;
  return init(nullptr, 0, dummy);
}

/// initialize a crypto buffer based on existing encrypted data
bool CryptoBuffer::init(const char *encData, int len, QByteArray &decData)
{
  bool exists = (encData && len);
  if (!exists) // new file, make salt and iv
  {
    // make salt
    if (RAND_bytes(d->keySalt, sizeof(d->keySalt)) <= 0)
      return DEBUGRET(false, "RAND_bytes failed to generate key salt");
    // make iv
    if (RAND_bytes(d->ivSalt, sizeof(d->ivSalt)) <= 0)
      return DEBUGRET(false, "RAND_bytes failed to generate iv");
  }
  else // existing file
  {
    // read salt and iv
    const size_t readlen = sizeof(d->keySalt) + sizeof(d->ivSalt);
    if (len < int(readlen))
      return DEBUGRET(false, "encData too short for salt and iv");

    memcpy(d->keySalt, encData, sizeof(d->keySalt));
    memcpy(d->ivSalt, encData + sizeof(d->keySalt), sizeof(d->ivSalt));

    encData += readlen;
    len -= readlen;
  }

  // generate the password-based key
  if (!PKCS5_PBKDF2_HMAC(d->password, d->password.size(), d->keySalt, sizeof(d->keySalt),
                         PBKDF_ITERATIONS, d->digest, sizeof(d->key), d->key))
    return DEBUGRET(false, "PKCS5_PBKDF2_HMAC failed for password");

  // generate the password-based iv
  if (!PKCS5_PBKDF2_HMAC(d->password, d->password.size(), d->ivSalt, sizeof(d->ivSalt),
                         PBKDF_ITERATIONS, d->digest, sizeof(d->iv), d->iv))
    return DEBUGRET(false, "PKCS5_PBKDF2_HMAC failed for iv");

  if (exists)
    return doCrypt(encData, len, false, decData);
  else
    return true;
}

bool CryptoBuffer::encrypt(const char *decData, int len, QByteArray &encData)
{
  // for encryption, prepend salt and iv data
  encData.append(reinterpret_cast<char*>(d->keySalt), sizeof(d->keySalt));
  encData.append(reinterpret_cast<char*>(d->ivSalt), sizeof(d->ivSalt));

  return doCrypt(decData, len, true, encData);
}

bool CryptoBuffer::doCrypt(const char *in, int inlen, bool isEncrypt, QByteArray &out)
{
  const int OUTBUF_SIZE = 8*1024;
  unsigned char outbuf[OUTBUF_SIZE + EVP_MAX_BLOCK_LENGTH];

  EVP_CIPHER_CTX ctx;
  EVP_CIPHER_CTX_init(&ctx);
  auto ctxGuard = makeGuard([&ctx] { EVP_CIPHER_CTX_cleanup(&ctx); });

  if (!EVP_CipherInit_ex(&ctx, d->cipher, NULL, d->key, d->iv, isEncrypt))
    return DEBUGRET(false, "EVP_CipherInit_Ex failed");

  const unsigned char *ptr = reinterpret_cast<const unsigned char*>(in);
  int restlen = inlen;
  int outlen;

  while (restlen > 0)
  {
    int readlen = std::min(restlen, OUTBUF_SIZE);
    if (!EVP_CipherUpdate(&ctx, outbuf, &outlen, ptr, readlen))
      return DEBUGRET(false, "EVP_CipherUpdate failed");

    out.append(reinterpret_cast<const char*>(outbuf), outlen);
    ptr += readlen;
    restlen -= readlen;
  }

  if (!EVP_CipherFinal_ex(&ctx, outbuf, &outlen))
    return DEBUGRET(false, "EVP_CipherFinal_ex failed");
  out.append(reinterpret_cast<const char*>(outbuf), outlen);

  return true;
}
