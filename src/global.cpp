#include "global.h"

bool validateName(const char *name)
{
  if (!name)
    return false;
  for (const char *p = name; *p; ++p)
    if (!((*p >= 'A' && *p <= 'Z') || (*p >= 'a' && *p <= 'z')))
      return false;
  return true;
}

bool validateToken(const char *token)
{
  if (!token)
    return false;
  for (const char *p = token; *p; ++p)
    if (!((*p >= 'A' && *p <= 'Z') || (*p >= 'a' && *p <= 'z') ||
          (*p >= '0' && *p <= '9')))
      return false;
  return true;
}
bool validateTime(Timestamp value)
{
  static_assert(1073741823 == (1 << 30) - 1, "just curious");
  return value > 0 && value <= 1073741823;
}

bool validateRoom(RoomID value)
{
  return value >= 0 && value <= 1073741823;
}
