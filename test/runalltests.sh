#!/bin/bash
rm -f log.txt
find ../testing_dist/tests/{core,optional}/ -name "*.json" | xargs -iX -t ./runtest.sh X 2>&1 | tee -a log.txt
